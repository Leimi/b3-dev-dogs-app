import React, {useEffect, useCallback, useState} from 'react';
import { Text, View, FlatList } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { useFocusEffect } from '@react-navigation/native';
import { getStorageItem, setStorageItem } from "../api/storage";
import ListItem from "./ListItem";

function FavoritesScreen() {
	const [favorites, setFavorites] = useState([])

	useFocusEffect(useCallback(() => {
		getStorageItem('favorites', []).then(setFavorites)
	}, []))

	if (!favorites.length) {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Text>You have no favorites yet</Text>
			</View>
		);
	}

	return (
		<View style={{flex: 1}}>
			<FlatList
				data={favorites}
				keyExtractor={({number}) => `${number}`}
				renderItem={({item, index}) => {
					return (
						<ListItem
							icon="trash"
							onIconPress={() => {
								const foundIndexInFavorites = favorites.findIndex(favorite => item.number === favorite.number)
								const newFavorites = [...favorites]
								newFavorites.splice(foundIndexInFavorites, 1)
								setStorageItem('favorites', newFavorites)
									.then(() => setFavorites(newFavorites))
							}}
						>
							<Text>{item.number}. {item.fact}</Text>
						</ListItem>
					)
				}}
			/>
		</View>
	)
}

// the stack navigator is here only to easily have a title bar
const FavoritesStack = createStackNavigator();

export default function FactsScreen() {
	return (
		<FavoritesStack.Navigator>
			<FavoritesStack.Screen
				name="Favorites"
				component={FavoritesScreen}
				options={() => ({
					title: 'Favorite facts'
				})}
			/>
		</FavoritesStack.Navigator>
	)
}