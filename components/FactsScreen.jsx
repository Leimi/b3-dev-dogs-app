import React, {useCallback, useState} from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { useFocusEffect } from '@react-navigation/native';
import { getStorageItem, setStorageItem } from "../api/storage";
import ListItem from "./ListItem";

const fetchFacts = () => {
	return fetch('https://dog-facts-api.herokuapp.com/api/v1/resources/dogs/all').then(response => {
		return response.json()
	}).then(json => {
		return json.map(({fact}, index) => ({
			fact,
			number: index + 1
		}))
	})
}

function FactsListScreen() {
	const [facts, setFacts] = useState([])
	const [favorites, setFavorites] = useState([])
	useFocusEffect(useCallback(() => {
		fetchFacts().then(setFacts)
		getStorageItem('favorites', []).then(setFavorites)
	}, []))
	return (
		<View style={{ flex: 1 }}>
			<FlatList
				data={facts}
				keyExtractor={({number}) => `${number}`}
				renderItem={({item, index}) => {
					const foundIndexInFavorites = favorites.findIndex(favorite => item.number === favorite.number)
					const isAlreadyInFavorites = foundIndexInFavorites !== -1
					const icon = isAlreadyInFavorites
						? 'heart'
						: 'heart-outline';
					return (
						<ListItem
							icon={icon}
							onIconPress={() => {
								if (isAlreadyInFavorites) {
									const newFavorites = [...favorites]
									newFavorites.splice(foundIndexInFavorites, 1)
									setStorageItem('favorites', newFavorites)
										.then(() => setFavorites(newFavorites))
								} else {
									setStorageItem('favorites', [...favorites, item])
										.then(() => getStorageItem('favorites', []))
										.then(setFavorites)
								}
							}}
						>
							<Text>{item.number}. {item.fact}</Text>
						</ListItem>
					)
				}}
			/>
		</View>
	);
}

// the stack navigator is here only to easily have a title bar
const FactsStack = createStackNavigator();

export default function FactsScreen() {
	return (
		<FactsStack.Navigator>
			<FactsStack.Screen name="FactsList" component={FactsListScreen} options={() => ({
				title: 'Dog Facts'
			})} />
		</FactsStack.Navigator>
	)
}